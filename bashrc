# -*- mode: sh -*-

# ------ # System Applications # ------ #

## modules
module load mendeley
module load vmd/1.9.2		# want this for catdcd, but superceded with Max's VMD
export PATH=/software/mendeley-1.13.4-x86_64/mendeleydesktop-1.13.4-linux-x86_64/bin:$PATH

## CUDA
module load cuda-toolkit/8.0
export CUDA_PATH=/software/cuda-toolkit-8.0


## Anaconda
export PATH="$PATH:/home/cmaffeo2/anaconda2/bin:$PATH" # you might want to install your own copy of anaconda! 

## Texlive
export TEXLIVE_INSTALL_PREFIX=/data/server5/cmaffeo2/texlive
INFOPATH=$TEXLIVE_INSTALL_PREFIX/2016/texmf-dist/doc/info:"$INFOPATH"
MANPATH=$TEXLIVE_INSTALL_PREFIX/2016/texmf-dist/doc/man:"$MANPATH"
PATH=$TEXLIVE_INSTALL_PREFIX/2016/bin/x86_64-linux:"$PATH"

## Simulation packages
alias namd2='/home/cmaffeo2/development/namd-bin/NAMD_2.12_Linux-x86_64-multicore-CUDA/namd2'
namd () {
    /home/cmaffeo2/development/namd-bin/NAMD_2.12_Linux-x86_64-multicore-CUDA/namd2 +ppn "$2" +netpoll +idlepoll +devices 1 "$1"
}

alias arbd='/home/cmaffeo2/development/cuda/arbd.dev/src/arbd'


# ---- # Bash Security Settings # ---- #

## File settings
umask 0022

## Bash History
# http://www.dufault.info/blog/logging-all-bash-commands-to-history-and-keeping-people-from-deletingemptying-it/
## append only: chattr +a .bash_history
## maybe in /etc/profile or /etc/bashrc
export HISTCONTROL=ignoreboth
export HISTSIZE=50000
export HISTFILESIZE=500000
export HISTTIMEFORMAT=%c

## Send tab title othe current path
PROMPT_COMMAND='echo -ne "\033]0;$(basename "$(pwd -P)")\007"'
# \033]0; is title+icon, \033]1; is icon, \033]2; is title
# \007 seems neccesary... I don't know why
case $HOSTNAME in
    YOURUSUALHOST*) PS1='\[\e[0;34m\][\j]\[\e[0;34m\] \w\[\e[0;m\] ';;
    *)      PS1='\[\e[0;34m\][\j]\[\e[0;32m\] $HOSTNAME\[\e[0;34m\] \w\[\e[0;m\] ';;
esac

if [ "$BASH" ]; then
    PROMPT_COMMAND="history -a;$PROMPT_COMMAND";
    readonly PROMPT_COMMAND
    readonly HISTSIZE
    readonly HISTFILE
    readonly HOME
    readonly HISTIGNORE
    readonly HISTCONTROL
fi

## aliases to stop you from accidental data loss, etc.
alias cp='cp -i'
alias rm='rm -i'		# override with -I
alias mv='mv -i'
alias killall='killall -i'

## Important enough to be here
alias e='emacs -nw'

# ---- # Bash Interface # ---- #
# export TERM="xterm-256color"
PATH="$HOME/bin:$PATH"
# export EDITOR="vim"
export EDITOR="emacsclient -t"                  # $EDITOR should open in terminal

## Shell Options
shopt -s extglob  ;# adds regexp-like globs (see man pages)
# shopt -s failglob ;# makes it so `echo nonExistentGlob.*` fails 

# set -C ;# If  set,  bash  does not overwrite an existing file with
#         #  the >, >&, and <> redirection operators.   This  may  be
#         #  overridden when creating output files by using the redi-
#         #  rection operator >| instead of >.

set -b ;# Report  the status of terminated background jobs immediately,
        #  rather than before the next primary prompt.

set +H  # Don't do annoying history expansions of exclamation points

if [[ -o emacs || -o vi ]]; then
    bind '"":history-search-backward'
    bind '"":history-search-forward'
    bind '"[5~":history-search-backward'
    bind '"[6~":history-search-forward'
fi

## Aliases for convenience
alias lls="ls -lht"
alias open='xdg-open'
alias vmd='vmd -nt'
alias vmdd="vmd -dispdev text"

alias echo='echo -e'
alias pd='pushd'
alias lns='ln -s'
alias mkdir='mkdir -p'
alias bc="bc -ql $HOME/.extensions.bc"
alias rsync='rsync -avP -e ssh'
alias nsync='ionice -c3 rsync -aH --info=progress2 -h -e ssh' # nice data copying

eval "`dircolors ~/.dotfiles/dircolors.ansi-dark`"
alias ls='ls --color=always'
alias grep='grep -i --color=auto'
alias tree='tree -FC'
alias less='less -R'

#-----# Network #-----#
for i in {2..16}; do
    eval "export work$i='$USER@tbgl-work$i.physics.illinois.edu'"
    eval "alias work$i='ssh \${work$i}'"
done
for i in 4 5 6; do
    eval "export server$i='$USER@tbgl-server$i.physics.illinois.edu'"
    eval "alias server$i='ssh \${server$i}'"
done
for i in "" 2; do
    eval "export gpu$i='$USER@tbgl-work-gpu$i.physics.illinois.edu'"
    eval "alias gpu$i='ssh \${gpu$i}'"
done

export anton="$USER@anton2.psc.edu"
export comet="$USER@comet.sdsc.xsede.org"
export bw="$USER@bw.ncsa.illinois.edu"
export titan="$USER@titan.ccs.ornl.gov"
export golub="$USER@golubh1.campuscluster.illinois.edu"
export stampede="$USER@stampede2.tacc.utexas.edu"
export ranch="$USER@ranch.tacc.utexas.edu"

alias anton='ssh $anton'
alias bw='ssh $bw'
alias titan='ssh $titan'
alias comet='ssh $comet'
alias golub='ssh $golub'
alias stampede='ssh $stampede'
alias ranch='ssh $ranch'

export bridges="$USER@bridges.psc.xsede.org"
alias bridges='ssh -p 2222 $bridges'
alias bridges-rsync="rsync -e ssh\ -p\ 2222"

# --- # Settings for programs # ---- #
export SVN=https://subversion.engr.illinois.edu/svn/tbgl/trunk
export SVNROOT=$SVN
export UNITSFILE=$HOME/.units
