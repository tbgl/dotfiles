;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Command line switches ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;--- ediff switch
(defun command-line-diff (switch)
  (let ((file1 (pop command-line-args-left))
        (file2 (pop command-line-args-left)))
    (ediff file1 file2)))
(add-to-list 'command-switch-alist '("-diff" . command-line-diff))
;; Usage: emacs -diff file1 file2

;;;;;;;;;;;;;;;;;;;;
;; Global hotkeys ;;
;;;;;;;;;;;;;;;;;;;;
(global-unset-key "\C-cC")
(global-unset-key "\C-cc")
(global-unset-key "\M-g")

;; http://www.cs.utah.edu/dept/old/texinfo/emacs19/emacs_35.html
(global-set-key [C-up] 'buffer-menu)

;; (global-set-key "\C-u" 'backward-kill-line)
(global-set-key "\C-cC" 'uncomment-region)
(global-set-key "\C-cc" 'comment-region)
(global-set-key "\M-g" 'goto-line)


;;;;;;;;;;;;;;
;; Packages ;;
;;;;;;;;;;;;;;

(if (>= emacs-major-version 24)
    (progn
      (require 'package)
      (add-to-list 'package-archives
		   '("melpa-stable" . "http://stable.melpa.org/packages/") t)
      (add-to-list 'load-path "~/.emacs.d/php-mode")
      (require 'php-mode)
      ))

;;--- comments
(add-to-list 'load-path "~/.emacs.d/rebox2")
(setq rebox-style-loop '(13 25 17))
(require 'rebox2)
(global-set-key [(meta q)] 'rebox-dwim)
(global-set-key [(shift meta q)] 'rebox-cycle)

(require 'saveplace)
(setq-default save-place t)
(setq save-place-file "~/.emacs.d/saved-places.txt")
(setq save-place-forget-unreadable-files nil) ;; uncomment if slow on NFS

(if (>= emacs-major-version 24)
    (progn
      (add-to-list 'custom-theme-load-path "~/.emacs.d/emacs-color-theme-solarized")
      (set-terminal-parameter nil 'background-mode 'dark) ; TODO: add logic for terminal
      (load-theme 'solarized t)
      (set-background-color "black")	; still a problem on iTerm over ssh?
      )
  (progn
    (add-to-list 'load-path "~/.emacs.d/color-theme")
    (add-to-list 'load-path "~/.emacs.d/emacs-color-theme-solarized")
    (setq solarized-degrade t)
    (set-terminal-parameter nil 'background-mode 'dark) ; TODO: add logic for terminal
    (require 'color-theme-solarized)
    ;; (setq color-theme-is-global t)
    ;; (color-theme-initialize)
    (color-theme-solarized)
    (set-background-color "black")	
   ))

;;-------- programming

(let ((default-directory  "~/.emacs.d/elpa/"))
  (normal-top-level-add-subdirs-to-load-path))

;;--- folding (outline minor modes)
(add-to-list 'load-path "~/.emacs.d/hideshow-org")
(require 'hideshow-org)
(add-hook 'outline-minor-mode-hook
	  (lambda () (local-set-key "\M-o"
				    outline-mode-prefix-map)))

(add-hook 'outline-minor-mode-hook 
	  '(lambda () 
	     ;;(global-set-key (kbd "<f7>")      'fold-dwim-toggle)
	     (global-set-key [Tab]      'fold-dwim-toggle)
	     (global-set-key (kbd "<S-Tab>")    'fold-dwim-hide-all)
	     (global-set-key (kbd "<S-C-Tab>")  'fold-dwim-show-all)
	     ))

;;-- outline mode
(add-hook 'outline-minor-mode-hook 
	  '(lambda ()
	     (global-set-key [M-left] 'hide-subtree)
	     (global-set-key [M-right] 'show-subtree)
	     (global-set-key [M-up] 'outline-previous-heading)
	     (global-set-key [M-down] 'outline-next-heading)
	     (global-set-key [C-left] 'hide-sublevels)
	     (global-set-key [C-down] 'outline-next-visible-heading)
	     (global-set-key [C-right] 'show-children)
	     (global-set-key [C-up] 'outline-previous-visible-heading)))

;;--- org-mode
(setq org-startup-indented t)
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(add-hook 'org-mode-hook
	  '(lambda ()
	     (global-set-key "\C-cl" 'org-store-link)
	     (global-set-key "\C-ca" 'org-agenda)
	     (global-set-key "\C-cb" 'org-iswitchb)
	     ;; (setq org-cycle-include-plain-lists t)
	     ))
(add-hook 'org-mode-hook 'turn-on-font-lock) ; Org buffers only
(require 'org-table)

(add-hook 'bibtex-mode-hook
	  '(lambda () (setq fill-column 3000)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(bibtex-align-at-equal-sign t)
 '(bibtex-entry-format
   (quote
    (opts-or-alts required-fields numerical-fields whitespace realign last-comma delimiters)))
 '(bibtex-field-delimiters (quote double-quotes))
 '(bibtex-files (quote ("." "/home/cmaffeo2/latexDocuments/bibfiles")))
 '(bibtex-string-files
   (quote
    ("/home/cmaffeo2/latexDocuments/bibfiles/journals_long.tex")))
 '(custom-safe-themes
   (quote
    ("8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" default)))
 '(inhibit-startup-screen t)
 )

; ----------- ;
; Programming ;
; ----------- ;

(add-to-list 'auto-mode-alist '("\\.inl\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cu\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cuh\\'" . c++-mode))

;; set Stroustrup-style C code
(setq c-default-style
      '((c-mode . "stroustrup") (c++-mode . "stroustrup")))

(setq load-path (cons "~/.emacs.d/lisp/" load-path))

;; display NAMD config files in Tcl mode
(setq auto-mode-alist (cons '("\\.namd\\'" . tcl-mode) auto-mode-alist))

;;gnuplot mode
(autoload 'gnuplot-mode "gnuplot" "gnuplot major mode" t)
(autoload 'gnuplot-make-buffer "gnuplot" "open a buffer in gnuplot mode" t)
(setq auto-mode-alist (append '(("\\.gp$" . gnuplot-mode)) auto-mode-alist))
(setq auto-mode-alist (append '(("\\.gnu$" . gnuplot-mode)) auto-mode-alist))

;;;;;;;;;;;;;;;;;;
;; New commands ;;
;;;;;;;;;;;;;;;;;;
(defun aj-toggle-fold ()
  "Toggle fold all lines larger than indentation on current line"
  (interactive)
  (let ((col 1))
    (save-excursion
      (back-to-indentation)
      (setq col (+ 1 (current-column)))
      (set-selective-display
       (if selective-display nil (or col 1))))))
(global-set-key [(M C i)] 'aj-toggle-fold)

(defun count-chars-region (beginning end)
  "Print number of characters in the region."
  (interactive "r")
  (message "Counting characters in region ... ")

;;; 1. Set up appropriate conditions.
  (save-excursion
    (let ((count 0))
      (goto-char beginning)

;;; 2. Run the while loop.
      (while (and (< (point) end)
                  (re-search-forward "." end t))
        (setq count (1+ count)))

;;; 3. Send a message to the user.
      (cond ((zerop count)
             (message
              "The region does NOT have any characters."))
            ((= 1 count)
             (message
              "The region has 1 character."))
            (t
             (message
              "The region has %d characters." count))))))

(defun count-words-region (beginning end)
  "Print number of words in the region."
  (interactive "r")
  (message "Counting words in region ... ")

;;; 1. Set up appropriate conditions.
  (save-excursion
    (let ((count 0))
      (goto-char beginning)

;;; 2. Run the while loop.
      (while (and (< (point) end)
                  (re-search-forward "\\w+\\W*" end t))
        (setq count (1+ count)))

;;; 3. Send a message to the user.
      (cond ((zerop count)
             (message
              "The region does NOT have any words."))
            ((= 1 count)
             (message
              "The region has 1 word."))
            (t
             (message
              "The region has %d words." count))))))

(defun count-words-buffer ()
  "Count all the words in the buffer"
  (interactive)
  (count-words-region (point-min) (point-max)))

(defun backward-kill-line (arg)
  "Kill chars backward until encountering the end of a line."
  (interactive "p")
  (kill-line 0))


;;;;;;;;;;;;;;;;;;;;;;
;; General Behavior ;;
;;;;;;;;;;;;;;;;;;;;;;
(push '("." . ".backup") backup-directory-alist)
(setq frame-title-format		; default to better frame titles
      (concat  "%b - emacs@" system-name))

(global-set-key [(meta /)] 'hippie-expand)
(setq vc-follow-symlinks t) ; prevent emacs from asking to follow git symlink
