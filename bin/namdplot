#!/bin/csh

# Written in 1999 by Jim Phillips, Theoretical Biophysics Group,
# Beckman Institute, University of Illinois at Urbana-Champaign.

if ( $#argv < 1 ) then
  echo "Usage: $0 [zero] [diff] [from <first>] [to <last>] [<yfield> [<yfield> ...] [vs <xfield>]] <file>"
  exit -1
endif

set titles = `awk '/^ETITLE:/ { print; exit }' $argv[$#argv]`
echo $titles

if ( $#argv < 2 ) then
  echo "Usage: $0 [zero] [diff] [from <first>] [to <last>] [<yfield> [<yfield> ...] [vs <xfield>]] <file>"
  exit -1
endif

if ( $1 == zero ) then
  set zero = 1
  shift argv
else
  set zero = 0
endif

if ( $1 == diff ) then
  set zero = 1
  set diff = 1
  shift argv
else
  set diff = 0
endif

if ( $1 == from ) then
  shift argv
  set first_step = $1
  shift argv
else
  set first_step = 0
endif

if ( $1 == to ) then
  shift argv
  set last_step = $1
  shift argv
else
  set last_step = 0
endif

set ytargets =
while ( $#argv > 1 && $1 != vs )
  if ( $1 == "--" ) then
    shift
    break
  endif
  @ pos = 1
  foreach t ( $titles )
    if ( $t == $1 ) set ytargets = ( $ytargets $pos )
    @ pos++
  end
  shift argv
end
if ( $#ytargets == 0 ) exit -1

set xtarget = 2
if ( $1 == vs ) then
  shift argv
  @ pos = 1
  foreach t ( $titles )
    if ( $t == $1 ) set xtarget = $pos
    @ pos++
  end
  shift argv
endif

set file = $argv[1]

set ytitle =
foreach y ( $ytargets )
  set ytitle = ( $ytitle $titles[$y] )
end
set xtitle = $titles[$xtarget]

echo Plotting $ytitle vs $xtitle

set title = $file

set open = '{'
set close = '}'
set dollar = '$'

if ( $zero ) then
  set prog = "BEGIN $open zset = 0 $close"
else
  set prog = ""
endif

if ( $first_step ) then
  set prog = "$prog ${dollar}2 < $first_step $open next $close ;"
endif

if ( $last_step ) then
  set prog = "$prog ${dollar}2 > $last_step $open next $close ;"
endif

if ( $zero ) then
  set subtitle = "change from initial value"
  set prog = "$prog zset == 0 $open zset = 1;"
  foreach y ( $ytargets )
    set prog = "$prog z$y = $dollar$y;"
  end
  set prog = "$prog $close"
  set prog = "$prog $open print $dollar$xtarget"
  foreach y ( $ytargets )
    set prog = "$prog, $dollar$y - z$y"
  end
  set prog = "$prog $close"
  if ( $diff ) then
    set subtitle = "running backward difference"
    set prog = "$prog $open"
    foreach y ( $ytargets )
      set prog = "$prog z$y = $dollar$y;"
    end
    set prog = "$prog $close"
  endif
else
  set subtitle = ""
  set prog = "$prog $open print $dollar$xtarget"
  foreach y ( $ytargets )
    set prog = "$prog, $dollar$y"
  end
  set prog = "$prog $close"
endif

set quote = '"'
set p_title = "TITLE $quote$title$quote"
set p_subtitle = "SUBTITLE $quote$subtitle$quote"
set p_xaxis = "XAXIS LABEL $quote$xtitle$quote"

set tmpfile = /tmp/namdplot.$USER.$$.tmp

set l_cmd = ( 0 0 0 0 0 0 0 0 0 0 )
@ pos = 1
foreach t ( $ytitle )
  @ i = $pos - 1
  set l_cmd[$pos] = "S$i LEGEND $quote$t$quote"
  @ pos++
end

while ( $pos <= 10 )
  set l_cmd[$pos] = "S$i LEGEND $quote$t$quote"
  @ pos++
end

cat $argv | grep '^ENERGY:' | grep -v '[a-z]' | awk "$prog" > $tmpfile

xmgrace \
  -pexec "LEGEND on" \
  -pexec "$l_cmd[1]" \
  -pexec "$l_cmd[2]" \
  -pexec "$l_cmd[3]" \
  -pexec "$l_cmd[4]" \
  -pexec "$l_cmd[5]" \
  -pexec "$l_cmd[6]" \
  -pexec "$l_cmd[7]" \
  -pexec "$l_cmd[8]" \
  -pexec "$l_cmd[9]" \
  -pexec "$l_cmd[10]" \
  -pexec "$p_title" -pexec "$p_subtitle" -pexec "$p_xaxis" \
  -pexec "XAXIS TICKLABEL FORMAT DECIMAL" -pexec "XAXIS TICKLABEL PREC 0" \
  -pexec "YAXIS TICKLABEL FORMAT DECIMAL" -pexec "YAXIS TICKLABEL PREC 0" \
  -autoscale xy -remove -nxy $tmpfile &

