#! /usr/bin/tclsh

## description:
## usage: getNamdOutputName.tcl config.namd

# set namdFile eq-heavy-2fs-2.namd
set namdFile [lindex $argv 0]

if { [llength $argv] != 1 } { error "Wrong number of arguments" }
if { ! [file exists $namdFile] } { error "$namdFile doesn't exist" }

## setup parser
set namdParser [interp create -safe]
# interp expose $namdParser open
interp hide $namdParser puts
foreach cmd {source pwd glob cd} {
    interp expose $namdParser $cmd
}

interp alias $namdParser open {} parserOpen $namdParser
proc parserOpen {parser file} {
    uplevel open $file r
}
proc parserOpen {parser file} {
    set ch [uplevel "open {$file} r"]
    interp share {} $ch $parser
    return $ch
}

## make puts commands work for debugging
interp alias $namdParser puts {} parserPuts $namdParser
interp alias $namdParser print {} parserPuts $namdParser
proc parserPuts {parser args} {
    # puts "$parser: $args"
}


## copy env to parser
foreach n [array names env] {
    interp eval $namdParser "set env($n) {$env($n)}"
}

## settup hook for outputname
interp alias $namdParser outputname {} foundNamdName
proc foundNamdName {name} {
    puts $name
    exit
}


## OutputName/outputname
interp eval $namdParser {
    proc unknown {cmd args} {
	puts "unkown: $cmd"
	## evaluate unknown commands without capitaliztion / ignore them 
	set newCmd [string tolower $cmd]
	if { ! [string equal $cmd $newCmd] } {
	    uplevel $newCmd $args
	} 
    }
}

# interp hide $namdParser puts
interp eval $namdParser "source $namdFile"
