#! /bin/bash

## 

gs \
  -o "$(basename $1 .pdf)-opt.pdf" \
  -sDEVICE=pdfwrite \
  -dDownsampleColorImages=true \
  -dDownsampleGrayImages=true \
  -dDownsampleMonoImages=true \
  -dColorImageResolution=300 \
  -dGrayImageResolution=600 \
  -dMonoImageResolution=600 \
  -f $1

  # -dConvertCMYKImagesToRGB=true \
  # -dDetectDuplicateImages=true \
